﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Helpers
{
    public class GiftingConfig
    {
        public string ConnectionString { get; set; }

        public string ApiLogTableName { get; set; }
        public string GiftingDbName { get; set; }
        public string SalesOrderApiUrl { get; set; }
        public int SalesOrderRetryCount { get; set; }
        public int SalesOrderTPS { get; set; }
        public string MongoHostName { get; set; }
        public int MongoHostPort { get; set; }
        public string XActionAPIParam { get; set; }
        public string Accept_Language { get; set; }
        public string Authorization { get; set; }
        public string X_Source_CountryCode { get; set; }
        public string X_Source_Operator { get; set; }
        public string X_Source_Division { get; set; }
        public string X_Source_System { get; set; }
        public string ShortCodeId { get; set; }
        public string DisableChargeAnswer { get; set; }
        public string RequestorMsisdn { get; set; }
    }
}
