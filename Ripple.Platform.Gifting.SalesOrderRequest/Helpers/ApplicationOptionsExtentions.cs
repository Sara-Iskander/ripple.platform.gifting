﻿using Core.Framewrok.Common.Contracts;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Helpers
{
    public static class ApplicationOptionsExtentions
    {

        public static T GetApplicationOptions<T>(this IApplicationOptions applicationOptions)
        {
            Type type = typeof(T);
            var instance = Activator.CreateInstance(type);
            PropertyInfo[] props = type.GetProperties();
            foreach (var prop in props)
            {
                Type fieldType = prop.PropertyType;
                MethodInfo _MethodInfoGetValue = typeof(IApplicationOptions).GetMethod($"{nameof(applicationOptions.GetValue)}").MakeGenericMethod(fieldType);
                prop.SetValue(instance, _MethodInfoGetValue.Invoke(applicationOptions, new object[] { prop.Name }));
            }
            return (T)instance;
        }

   
    }
}
