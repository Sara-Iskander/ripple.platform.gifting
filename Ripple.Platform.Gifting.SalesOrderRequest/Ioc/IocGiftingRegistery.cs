﻿using Core.Framework.Common.Db.MongoDb;
using Core.Framework.Logging.Abstractions;
using Core.Framewrok.Common.Contracts;
using MongoDB.Driver;
using Ripple.Common.Network.Models;
using Ripple.Common.Presistance.Repository.Write;
using Ripple.Platform.Gifting.Presistance.Mongo.Ioc;
using Ripple.Platform.Gifting.SalesOrderRequest.Helpers;
using Ripple.Platform.Gifting.SalesOrderRequest.Network;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Ioc
{
    public class IocGiftingRegistery : Registry
    {
        private readonly string  _strActualGiftingApiClient = "ActualGiftingClient";

        public IocGiftingRegistery(IApplicationOptions applicationOptions, ILogger logger)
        {
            GiftingConfig _GiftingConfig = applicationOptions.GetApplicationOptions<GiftingConfig>();
         


            this.For<ISalesOrderApiClient>().Use<SalesOrderApiClient>()
                    .Ctor<string>("baseUrl").Is(_GiftingConfig.SalesOrderApiUrl)
                    .Ctor<int>("salesOrderRetryCount").Is(_GiftingConfig.SalesOrderRetryCount)
                    .Ctor<string>("xActionAPIParam").Is(_GiftingConfig.XActionAPIParam)
                    .Ctor<string>("x_Source_System").Is(_GiftingConfig.X_Source_System)
                    .Ctor<string>("x_Source_Division").Is(_GiftingConfig.X_Source_Division)
                    .Ctor<string>("x_Source_Operator").Is(_GiftingConfig.X_Source_Operator)
                    .Ctor<string>("x_Source_CountryCode").Is(_GiftingConfig.X_Source_CountryCode)
                    .Ctor<string>("authorization").Is(_GiftingConfig.Authorization)
                    .Ctor<string>("accept_Language").Is(_GiftingConfig.Accept_Language)
                    .Ctor<string>("shortCodeId").Is(_GiftingConfig.ShortCodeId)
                    .Ctor<string>("disableChargeAnswer").Is(_GiftingConfig.DisableChargeAnswer)
                    .Ctor<string>("requestorMsisdn").Is(_GiftingConfig.RequestorMsisdn)

                    .Named(_strActualGiftingApiClient);

            this.For<ISalesOrderApiClient>().Use<SalesOrderApiClientTPSProxy>().
                     Ctor<ISalesOrderApiClient>("apiClient").IsNamedInstance(_strActualGiftingApiClient)
                    .Ctor<int>("TPS").Is(_GiftingConfig.SalesOrderTPS);

            this.IncludeRegistry(new MongoRepositoriesIocRegistery(_GiftingConfig.GiftingDbName, _GiftingConfig.ApiLogTableName, _GiftingConfig.ConnectionString));
        }
    }
}
