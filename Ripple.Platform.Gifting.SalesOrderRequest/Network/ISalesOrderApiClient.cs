﻿using Ripple.Common.Responses;
using Ripple.Platform.Gifting.SalesOrderRequest.Network.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Network
{
    public interface ISalesOrderApiClient
    {
        Task<CommonResponse<bool, SalesOrderRequestResponseEnum>> SendGift(string msisdn, string messageId);

    }
}
