﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Network.Models.Response
{
    public class SalesOrderResponseModel
    {
        public const string RESULT_ERROR_CODE_HEADER = "X-ResultStatus-ErrorCode";
        public const string RESULT_ERROR_MESSAGE_HEADER = "X-ResultStatus-Message";
        public const string RESULT_SUCCESS_CODE = "20900";
    }
}
