﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Network.Models
{
    public enum SalesOrderRequestResponseEnum
    {
        Success = 1,
        Error,
        ValidationError,
        NoDataFound,
        ApiError,
        ExceededTPS,
    }
}
