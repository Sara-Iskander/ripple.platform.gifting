﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ripple.Common.Responses;
using Ripple.Platform.Gifting.SalesOrderRequest.Network.Models;
using Core.Framework.Logging.Abstractions;
using System.Timers;
using System.Net;
using System.Threading;
using System.Diagnostics;
using Core.Framewrok.Common.Contracts.Response;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Network
{
    public class SalesOrderApiClientTPSProxy : ISalesOrderApiClient
    {

        private ISalesOrderApiClient _apiClient;
        private int _intTPS;
        private static System.Timers.Timer _timer;
        private static int _intcounter;
        private const string DEBUG_CATEGROTY = "TPS";
        static SalesOrderApiClientTPSProxy()
        {
            _timer = new System.Timers.Timer();
            _timer.Interval = 1000;//second
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _intcounter = 0;
        }

        public SalesOrderApiClientTPSProxy(ISalesOrderApiClient apiClient, int balanceTPS)
        {
            this._apiClient = apiClient;
            this._intTPS = balanceTPS;
        }

        public async Task<CommonResponse<bool, SalesOrderRequestResponseEnum>> SendGift(string msisdn, string messageId)
        {
            ServicePointManager.DefaultConnectionLimit = _intTPS;
            CommonResponse<bool, SalesOrderRequestResponseEnum> _Return = null;
            Interlocked.Increment(ref _intcounter);
            if (_intcounter <= _intTPS || _intTPS == 0)//equal zero if we need to disable TPS
            {
                Debug.WriteLine($"Tps current {_intcounter}", DEBUG_CATEGROTY);
                _Return = await _apiClient.SendGift(msisdn,messageId);
            }
            else
            {
                Debug.WriteLine($"Tps Exceeded {_intcounter}", DEBUG_CATEGROTY);
                _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(new List<Error>() { new Error("SalesOrderApiClientTPSProxy", $"SalesOrder exceeded TPS .The current requests is {_intcounter}") },
                    new CrudResponse<bool, SalesOrderRequestResponseEnum>(SalesOrderRequestResponseEnum.ExceededTPS));
            }
            return _Return;
        }
    }
}
