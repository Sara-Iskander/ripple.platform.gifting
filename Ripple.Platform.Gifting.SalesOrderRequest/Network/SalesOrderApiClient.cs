﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ripple.Common.Responses;
using Ripple.Platform.Gifting.SalesOrderRequest.Network.Models;
using Ripple.Common.Network;
using Core.Framework.Logging.Abstractions;
using Ripple.Common.Presistance.Repository.Write;
using Ripple.Common.Presistance.Entities.Write;
using RestSharp;
using Ripple.Platform.Gifting.SalesOrderRequest.Network.Models.Response;
using Core.Framewrok.Common.Contracts.Response;
using Ripple.Platform.Gifting.SalesOrderRequest.Network.Models.Request;
using Core.Framework.Common.Helpers;
using System.Linq;

namespace Ripple.Platform.Gifting.SalesOrderRequest.Network
{
    public class SalesOrderApiClient : ApiClient<string>, ISalesOrderApiClient
    {
        private ILogger _logger;

        private int _salesOrderRetryCount;
        private string _strXActionAPIParam;
        private string Accept_Language { get; set; }
        private string Authorization { get; set; }
        private string X_Source_CountryCode { get; set; }
        private string X_Source_Operator { get; set; }
        private string X_Source_Division { get; set; }
        private string X_Source_System { get; set; }
        private string ShortCodeId { get; set; }
        private string DisableChargeAnswer { get; set; }
        private string RequestorMsisdn { get; set; }

        public SalesOrderApiClient(string baseUrl, int salesOrderRetryCount, string xActionAPIParam, IApiLoger<IApiLog<string>> apiLoger,string x_Source_System, string x_Source_Division,string x_Source_Operator,string x_Source_CountryCode,string authorization,
            string accept_Language,
            string shortCodeId,
            string disableChargeAnswer,
            string requestorMsisdn) : base(baseUrl, apiLoger)
        {
            this._salesOrderRetryCount = salesOrderRetryCount;
            this._strXActionAPIParam = xActionAPIParam;
            Accept_Language = accept_Language;
            Authorization = authorization;
            X_Source_CountryCode = x_Source_CountryCode;
            X_Source_Operator = x_Source_Operator;
            X_Source_Division = x_Source_Division;
            X_Source_System = x_Source_System;
            RequestorMsisdn = requestorMsisdn;
            ShortCodeId = shortCodeId;
            DisableChargeAnswer = disableChargeAnswer;
        }

        public async Task<CommonResponse<bool, SalesOrderRequestResponseEnum>> SendGift(string msisdn,string messageId)
        {
            SalesOrderRequestModel queryBalanceRequestModel = new SalesOrderRequestModel(RequestorMsisdn,msisdn,ShortCodeId,DisableChargeAnswer);

            CommonResponse<bool, SalesOrderRequestResponseEnum> _Return = await CallSalesOrderApiWithRetry(queryBalanceRequestModel, messageId);
            return _Return;

            
        }
        private async Task<CommonResponse<bool, SalesOrderRequestResponseEnum>> CallSalesOrderApiWithRetry(SalesOrderRequestModel model, string messageId, int retryCount = 0)
        {
            var resultOpResult = await SendSalesOrderPrivate(model, messageId);
            if (!resultOpResult.IsValid() && resultOpResult.Result.Code == SalesOrderRequestResponseEnum.Error)
            {
                if (retryCount <= _salesOrderRetryCount)
                {
                    retryCount += 1;
                    resultOpResult = await CallSalesOrderApiWithRetry(model, messageId, retryCount);
                }
            }
            return resultOpResult;
        }

        private async Task<CommonResponse<bool, SalesOrderRequestResponseEnum>> SendSalesOrderPrivate(SalesOrderRequestModel model, string messageId)
        {
            CommonResponse<bool, SalesOrderRequestResponseEnum> _Return = null;
            try
            {

                var RestResponseResult = await this.CustomPostJson<List<SalesOrderResponseModel>>("service/service-balances", model, messageId);
                if (RestResponseResult != null)
                {
                    if (RestResponseResult.StatusCode == System.Net.HttpStatusCode.OK || RestResponseResult.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        var SalesOrderApiResultCode = RestResponseResult.Headers.Where(d => d.Name == SalesOrderResponseModel.RESULT_ERROR_CODE_HEADER).FirstOrDefault()?.Value?.ToString();
                        if (SalesOrderApiResultCode == SalesOrderResponseModel.RESULT_SUCCESS_CODE)
                        {
                            _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(new CrudResponse<bool, SalesOrderRequestResponseEnum>(true, SalesOrderRequestResponseEnum.Success));
                        }
                        else
                        {
                            var SalesOrderApiResultMessage = RestResponseResult.Headers.Where(d => d.Name == SalesOrderResponseModel.RESULT_ERROR_MESSAGE_HEADER).FirstOrDefault()?.Value?.ToString();
                            _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(
                              new List<Error> { new Error("SalesOrderApiClient", $"{SalesOrderApiResultCode} : {SalesOrderApiResultMessage}") }
                              , new CrudResponse<bool, SalesOrderRequestResponseEnum>(false, SalesOrderRequestResponseEnum.ApiError));
                        }

                    }
                    else
                    {
                        //X-ResultStatus-Message  
                        var QueryBalanceApiResultCode = RestResponseResult.Headers.Where(d => d.Name == SalesOrderResponseModel.RESULT_ERROR_CODE_HEADER).FirstOrDefault()?.Value?.ToString();
                        var QueryBalanceApiResultMessage = RestResponseResult.Headers.Where(d => d.Name == SalesOrderResponseModel.RESULT_ERROR_MESSAGE_HEADER).FirstOrDefault()?.Value?.ToString();
                        _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(
                          new List<Error> { new Error("SalesOrderApiClient", $" Result code [ {QueryBalanceApiResultCode} ]: {QueryBalanceApiResultMessage}") }
                          , new CrudResponse<bool, SalesOrderRequestResponseEnum>(false, SalesOrderRequestResponseEnum.ApiError));
                    }
                }
                if (_Return == null) //nothing matched the search criteria 
                {
                    _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(
                        new List<Error> { new Error("SalesOrderApiClient", "no data found ") }
                        , new CrudResponse<bool, SalesOrderRequestResponseEnum>(true, SalesOrderRequestResponseEnum.NoDataFound));

                }

            }
            catch (Exception e)
            {

                _Return = new CommonResponse<bool, SalesOrderRequestResponseEnum>(new List<Error> { new Error("SalesOrderApiClient", "failed to query") }
                , new CrudResponse<bool, SalesOrderRequestResponseEnum>(SalesOrderRequestResponseEnum.Error));

                _logger.LogErrorAsync(e, $"{nameof(SalesOrderApiClient)}.{nameof(SendGift)}");
            }

            return _Return;
        }
            private Task<IRestResponse<T>> CustomPostJson<T>(string resourceUrl, object obj, string messageId) where T : new()
        {
            var request = new RestRequest(Method.POST);
            request.Resource = resourceUrl;

            if (obj != null)
            {
                request.JsonSerializer = new ourSerilizable
                {
                    ContentType = request.JsonSerializer.ContentType,
                    DateFormat = request.JsonSerializer.DateFormat,
                    Namespace = request.JsonSerializer.Namespace,
                    RootElement = request.JsonSerializer.RootElement
                };
                request.AddJsonBody(obj);
            }
            return ExecuteRequestAsync<T>(request, messageId);
        }

        private Task<IRestResponse<T>> ExecuteRequestAsync<T>(RestSharp.RestRequest request, string messageId) where T : new()
        {
            request.AddHeader("X-Action", _strXActionAPIParam);
            request.AddHeader("X-MessageID", messageId);
            request.AddHeader("X-Source-CountryCode", X_Source_CountryCode);
            request.AddHeader("X-Source-Operator",X_Source_Operator);
            request.AddHeader("X-Source-Division",X_Source_Division);
            request.AddHeader("X-Source-System",X_Source_System);
            request.AddHeader("X-Source-Timestamp", DateTime.Now.ToIsoDateTime());


            return ExecuteAsyncReturnResponse<T>(request);
        }

    }
}
