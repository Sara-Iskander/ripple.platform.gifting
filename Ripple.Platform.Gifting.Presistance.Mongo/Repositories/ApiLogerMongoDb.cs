﻿using Ripple.Common.Presistance.Entities.Write;
using Ripple.Common.Presistance.Repository.Write;
using Ripple.Platform.Gifting.Presistance.Common.Repositories;
using Ripple.Platform.Gifting.Presistance.Mongo.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Repositories
{
    public class ApiLogerMongoDb : IApiLoger<IApiLog<string>>
    {
        private IApiLogRepository<ApiLog, string> _respository;

        public ApiLogerMongoDb(IApiLogRepository<ApiLog, string> repository)
        {
            this._respository = repository;
        }
        public void Log(IApiLog<string> obj)
        {
            var ApiLogMongoEntity = new ApiLog
            {
                BsonId = Guid.NewGuid().ToString(),
                RequestDateTime = obj.RequestDateTime,
                RequestContentType = obj.RequestContentType,
                RequestContentBody = obj.RequestContentBody,
                RequestHttpMethod = obj.RequestHttpMethod,
                RequestUri = obj.RequestUri,
                RequestHeader = obj.RequestHeader,

                ResponseContentType = obj.ResponseContentType,
                ResponseContentBody = obj.ResponseContentBody,
                ResponseDateTime = obj.ResponseDateTime,
                ResponseStatusCode = obj.ResponseStatusCode,
                ResponseHeader = obj.ResponseHeader,
                Milliseconds = obj.Milliseconds
            };
            _respository.Add(ApiLogMongoEntity);
        }
    }
}
