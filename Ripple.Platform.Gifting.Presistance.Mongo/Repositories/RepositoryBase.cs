﻿using Core.Framework.Common.Db.MongoDb;
using Core.Framework.Logging.Abstractions;
using Core.Framewrok.Common;
using Core.Framewrok.Common.Contracts.Persistence.Entities.Read;
using Core.Framewrok.Common.Contracts.Persistence.Entities.Write;
using Core.Framewrok.Common.Contracts.Persistence.Repository.Read;
using Core.Framewrok.Common.Contracts.Persistence.Repository.Write;
using Core.Framewrok.Common.Contracts.Response;
using MongoDB.Bson;
using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Repositories
{
    public class RepositoryBase<T, TId> : IReadRepository<T, TId>, IWriteRepository<T, TId>
        where T : class, IReadEntity<TId>, IWriteEntity<TId>, IMongoEntity
    {

        protected IMongoStorage<T> _MongoStorage;
        protected readonly ILogger _Logger;
        public RepositoryBase(IMongoStorage<T> mongoStorage, ILogger logger)
        {
            _MongoStorage = mongoStorage;

            _Logger = logger;
        }

        public async Task<OperationResult<bool>> Add(T newEntity)
        {
            try
            {
                //   newEntity.CreationDate = DateTime.Now;
                await _MongoStorage.InsertAsync(newEntity);

                return new OperationResult<bool>(OperationStatusEnum.Completed, true, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                //return new OperationResult<bool>(OperationStatusEnum.Duplication, false,
                if (exception.GetType() == typeof(MongoWriteException))
                {
                    var writeException = ((MongoWriteException)exception);
                    if (writeException.WriteError.Code == 11000)
                    {
                        return new OperationResult<bool>(OperationStatusEnum.Duplication, false,
                            new List<Error>
                                {
                                    new Error(
                                        $"Ripple.Platform.Contexts.Gifting.Presistance.MongoDb.Repositories.{nameof(T)}.{nameof(Add)}","duplicate id",
                                        exception)
                                });
                    }
                    else
                    {
                        return new OperationResult<bool>(OperationStatusEnum.Error, false,
                             new List<Error>
                                {
                                    new Error(
                                        $"Ripple.Platform.Contexts.Gifting.Presistance.MongoDb.Repositories.{nameof(T)}.{nameof(Add)}",
                                        exception)
                                });
                    }
                }
                return new OperationResult<bool>(OperationStatusEnum.Error, false,
                 new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(Add)}",
                            exception)
                    });
            }
        }
        public async Task<OperationResult<bool>> Update(T updatedEntity)
        {
            OperationResult<bool> response;
            try
            {
                // updatedEntity.UpdateDate = DateTime.Now;
                var DbEntity = await Get(updatedEntity.Id);

                if (DbEntity.Result.ValidVersion(updatedEntity.Version))
                {
                    updatedEntity.IncrementVersion();
                    // updatedEntity.UpdateDate = DateTime.Now;
                    //updatedEntity.CreationDate = DbEntity.Result.CreationDate;

                    await _MongoStorage.ReplaceAsync(updatedEntity.BsonId, updatedEntity);

                    response = new OperationResult<bool>(OperationStatusEnum.Completed, true, null);
                }
                else
                {

                    response = new OperationResult<bool>(OperationStatusEnum.VersionChanged, false,
                   new List<Error>
                   {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(Update)}",$"entity is updated by another user .current version is [{DbEntity.Result.Version}]")
                   });

                }

            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                // response = new OperationResult<bool>(OperationStatusEnum.DbError, false,
                return new OperationResult<bool>(OperationStatusEnum.Error, false,
                  new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Contexts.Gifting.Presistance.MongoDb.Repositories.{nameof(T)}.{nameof(Update)}",
                            exception)
                    });
            }
            return response;
        }


        public async Task<OperationResult<bool>> Delete(T deletedEntity)
        {
            try
            {
                await _MongoStorage.DeleteAsync(deletedEntity.BsonId);

                return new OperationResult<bool>(OperationStatusEnum.Completed, true, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<bool>(OperationStatusEnum.Error, false,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(Delete)}",
                            exception)
                    });
            }
        }

        public async Task<OperationResult<T>> FindById(TId id)
        {
            try
            {
                var result = await _MongoStorage.GetDataAsync(id.ToString());

                if (result != null)
                    return new OperationResult<T>(
                        OperationStatusEnum.Completed, result, null);

                return new OperationResult<T>(
                    OperationStatusEnum.DoesntExist, null, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<T>(OperationStatusEnum.Error, null,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(FindById)}({id})",
                            exception)
                    });
            }
        }

        public async Task<OperationResult<T>> Get(TId id)
        {
            try
            {
                var result = await _MongoStorage.GetDataAsync(id.ToString());

                if (result != null)
                    return new OperationResult<T>(
                        OperationStatusEnum.Completed, result, null);

                return new OperationResult<T>(
                    OperationStatusEnum.DoesntExist, null, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<T>(OperationStatusEnum.Error, null,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(Get)}({id})",
                            exception)
                    });
            }
        }

        public async Task<OperationResult<List<T>>> GetAllAsync()
        {
            try
            {
                var result = await _MongoStorage.GetDataAsync();

                if (result != null)
                    return new OperationResult<List<T>>(
                        OperationStatusEnum.Completed, result.ToList(), null);

                return new OperationResult<List<T>>(
                    OperationStatusEnum.DoesntExist, null, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<List<T>>(OperationStatusEnum.Error, null,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(GetAllAsync)}",
                            exception)
                    });
            }
        }


        public async Task<OperationResult<List<T>>> GetAllAsync(T searchPredicate)
        {
            try
            {
                var result = await _MongoStorage.GetDataAsync(searchPredicate.ToBsonDocument());

                if (result != null)
                    return new OperationResult<List<T>>(
                        OperationStatusEnum.Completed, result.ToList(), null);

                return new OperationResult<List<T>>(
                    OperationStatusEnum.DoesntExist, null, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<List<T>>(OperationStatusEnum.Error, null,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(GetAllAsync)}",
                            exception)
                    });
            }
        }



        public async Task<OperationResult<List<T>>> GetAllAsync(Expression<Func<T, bool>> searchPredicate)
        {
            try
            {
                var result = await _MongoStorage.GetDataAsync(searchPredicate);

                if (result != null && result.Count() > 0)
                    return new OperationResult<List<T>>(
                        OperationStatusEnum.Completed, result.ToList(), null);

                return new OperationResult<List<T>>(
                    OperationStatusEnum.DoesntExist, null, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                return new OperationResult<List<T>>(OperationStatusEnum.Error, null,
                    new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Gifting.Presistance.Mongo.Repositories.{nameof(T)}.{nameof(GetAllAsync)}",
                            exception)
                    });
            }
        }
    }
}
