﻿using Core.Framework.Common.Db.MongoDb;
using Core.Framework.Logging.Abstractions;
using Core.Framewrok.Common;
using Core.Framewrok.Common.Contracts.Response;
using MongoDB.Driver;
using Ripple.Platform.Gifting.Presistance.Common.Repositories;
using Ripple.Platform.Gifting.Presistance.Mongo.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Repositories
{
    public class ApiLogRepository : RepositoryBase<ApiLog, string>, IApiLogRepository<ApiLog, string>
    {
        public ApiLogRepository(IMongoStorage<ApiLog> mongoStorage, ILogger logger) : base(mongoStorage, logger)
        {

        }

        public async Task<OperationResult<bool>> LogResponse(ApiLog updatedEntity)
        {
            OperationResult<bool> response = null;
            try
            {

                var query = Builders<ApiLog>.Filter.Eq(d => d.BsonId, updatedEntity.BsonId);

                var update = Builders<ApiLog>.Update
                    .Set(d => d.ResponseContentBody, updatedEntity.ResponseContentBody)
                    .Set(d => d.ResponseContentType, updatedEntity.ResponseContentType)
                    .Set(d => d.ResponseDateTime, updatedEntity.ResponseDateTime)
                    .Set(d => d.ResponseStatusCode, updatedEntity.ResponseStatusCode);

                _MongoStorage.Collection.FindOneAndUpdate(query, update);
                response = new OperationResult<bool>(OperationStatusEnum.Completed, true, null);
            }
            catch (Exception exception)
            {
                _Logger.LogErrorAsync(exception);
                // response = new OperationResult<bool>(OperationStatusEnum.DbError, false,
                response = new OperationResult<bool>(OperationStatusEnum.Error, false,
                  new List<Error>
                    {
                        new Error(
                            $"Ripple.Platform.Billing.Presistance.Mongo.Repositories.{nameof(ApiLog)}.{nameof(LogResponse)}",
                            exception)
                    });
            }
            return response;
        }
    }
}
