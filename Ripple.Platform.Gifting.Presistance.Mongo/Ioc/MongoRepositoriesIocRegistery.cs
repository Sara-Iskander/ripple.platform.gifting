﻿using Core.Framework.Common.Db.MongoDb;
using MongoDB.Driver;
using Ripple.Common.Network.Models;
using Ripple.Common.Presistance.Repository.Write;
using Ripple.Platform.Gifting.Presistance.Common.Repositories;
using Ripple.Platform.Gifting.Presistance.Mongo.Entities;
using Ripple.Platform.Gifting.Presistance.Mongo.Repositories;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Ioc
{
    public class MongoRepositoriesIocRegistery : Registry
    {
        public MongoRepositoriesIocRegistery(string DbName,string apiLogTableName,string connectionString)
        {
            var ApiLogMongoStorage = new MongoStorage<ApiLog>(new MongoClient(connectionString), apiLogTableName,DbName);

            this.For<IMongoStorage<ApiLog>>().Use(ApiLogMongoStorage).Singleton();

            this.For<IApiLogRepository<ApiLog, string>>()
                                    .Use<ApiLogRepository>();

            this.For<IApiLoger<Ripple.Common.Presistance.Entities.Write.IApiLog<string>>>()
                           .Use<ApiLogerMongoDb>();

            
        }
    }
}
