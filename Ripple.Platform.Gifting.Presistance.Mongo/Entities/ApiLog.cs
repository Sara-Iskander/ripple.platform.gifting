﻿using Ripple.Platform.Gifting.Presistance.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Entities
{
    public partial class ApiLog : WriteMongoEntity, IApiLog<string>
    {
        public string RequestUser { get; set; }                    // The user that made the request.

        public string RequestMachine { get; set; }                 // The machine that made the request.

        public string RequestIpAddress { get; set; }        // The IP address that made the

        public string RequestContentType { get; set; }      // The request content type.

        public string RequestContentBody { get; set; }      // The request content body.

        public string RequestUri { get; set; }              // The request URI.

        public string RequestHttpMethod { get; set; }           // The request method (GET, POST, etc).

        public DateTime RequestDateTime { get; set; }     // The request timestamp.

        public string ResponseContentType { get; set; }     // The response content type.

        public string ResponseContentBody { get; set; }     // The response content body.

        public int? ResponseStatusCode { get; set; }        // The response status code.

        public DateTime? ResponseDateTime { get; set; }    // The response timestamp.
        public double Milliseconds { get; set; }
        public string RequestHeader { get; set; }
        public string ResponseHeader { get; set; }
        string Core.Framewrok.Common.Contracts.Persistence.Entities.IEntity<string>.Id { get => this.BsonId; set => this.BsonId = value; }

    }
}
