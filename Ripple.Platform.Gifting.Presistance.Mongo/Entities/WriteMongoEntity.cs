﻿using Core.Framework.Common.Db.MongoDb;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.Presistance.Mongo.Entities
{
    public abstract class WriteMongoEntity : MongoEntity, IMongoEntity
    {
        public int Version { get; set; }

        [BsonIgnore]
        public string Id => BsonId;

        /// <summary>
        /// Increments the version.
        /// </summary>
        public virtual void IncrementVersion()
        {
            Version = Version + 1;
        }

        /// <summary>
        /// Valids the version.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns></returns>
        public virtual bool ValidVersion(int version)
        {
            return Version == version;
        }

        //public DateTime CreationDate { get; set; }
        //public DateTime? UpdateDate { get; set; }

        public bool IsDeleted { get; private set; }

        public void MarkAsDeleted()
        {
            IsDeleted = true;
            // IncrementVersion();
        }
    }
}
