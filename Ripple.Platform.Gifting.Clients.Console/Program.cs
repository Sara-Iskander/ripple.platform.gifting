﻿using Core.Framework.Logging.Abstractions;
using Core.Framework.Logging.Console;
using Core.Framework.Logging.Repos.Database.MongoDb;
using Core.Framewrok.Common.Contracts;
using Microsoft.Extensions.Configuration;
using Ripple.Common.Options;
using Ripple.Common.Presistance.Repository.Write;
using Ripple.Platform.Gifting.SalesOrderRequest.Ioc;
using Ripple.Platform.Gifting.SalesOrderRequest.Network;
using StructureMap;
using System;
using System.IO;

namespace Ripple.Platform.Gifting.Clients.Console
{
    public class Program
    {
        public static ISalesOrderApiClient GetSaleOrderApiClient()
        {
            IConfiguration configuration = Configure();
           

            ILoggerFactory loggerFactory = new Core.Framework.Logging.LoggerFactory();

            loggerFactory.AddCoreLoggerForConsole();
            loggerFactory.AddCoreLoggerForMongo(configuration);

            Core.Framework.Logging.Abstractions.ILogger logger = loggerFactory.CreateLogger("Voting.Clients.Subsriber");

            var container = new Container();

            container.Configure(config =>
            {
                config.Scan(_ =>
                {
                    _.WithDefaultConventions();
                });

                config.For(typeof(IApplicationOptions))
                    .Use(new ApplicationOptions("GiftingContext", configuration, logger)).Singleton();

                config.For(typeof(Core.Framework.Logging.Abstractions.ILogger)).Use(logger).Singleton();


                config.IncludeRegistry(
                    new IocGiftingRegistery(
                        new ApplicationOptions("GiftingContext", configuration, logger),
                        logger));


                // Populate the container using the service collection
               
            });

            //IApplicationOptions applicationOptions = container.GetInstance<IApplicationOptions>();
            var repo = container.GetInstance<IApiLoger<Common.Presistance.Entities.Write.IApiLog<string>>>();
            return container.GetInstance<ISalesOrderApiClient>();
        }
        public static IConfiguration Configure()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            return builder.Build();
        }
        static void Main(string[] args)
        {
            var apiClient = GetSaleOrderApiClient();
            var result = apiClient.SendGift("1234", Guid.NewGuid().ToString()).Result;
            //for(int i = 0; i < 10; i++)
            //{
            //    result = apiClient.SendGift("1234", Guid.NewGuid().ToString()).Result;
            //}
            System.Console.WriteLine("Hello World!");
            System.Console.ReadLine();

        }
    }
}
