﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ripple.Platform.Gifting.Clients.Api.Models
{
    public class SendGiftParam
    {
        public SendGiftParam(string RequestorMsisdn, string msisdn, string shortCodeId, string disableChargeAnswer)
        {
            part = new Part() { line_items = new List<line_items>() { new line_items() { categories = new List<Categories>() { new Categories() { listName = "disable-charge", Value = disableChargeAnswer } }, payment = new payment() { billing_Account_Id = new billing_Account_Id() { name = "airtime" } }, product_Offerings = new List<product_offerings>() { new product_offerings() { ids = new List<id>() { new id() { Scheme_Id = "shortcode-id", Value = shortCodeId } } } } } } };
            role = new Role() { Requestor = new requestor() { Ids = new List<id>() { new id() { Scheme_Id = "msisdn", Value = RequestorMsisdn } } }, subscriber = new subscriber() { Ids = new List<id>() { new id() { Scheme_Id = "msisdn", Value = msisdn } } } };
        }
        [JsonProperty("parts")]
        public Part part { get; set; }
        [JsonProperty("roles")]
        public Role role { get; set; }
    }
    public class Role
    {
        public requestor Requestor { get; set; }
        public subscriber subscriber { get; set; }
    }
    public class Part
    {
        [JsonProperty("line-items")]
        public List<line_items> line_items { get; set; }
    }
    public class id
    {
        [JsonProperty("$")]
        public string Value { get; set; }
        [JsonProperty("@scheme-id")]
        public string Scheme_Id { get; set; }
    }
    public class requestor
    {
        [JsonProperty("ids")]
        public List<id> Ids { get; set; }
    }
    public class subscriber
    {
        [JsonProperty("ids")]
        public List<id> Ids { get; set; }

    }

    public class payment
    {
        [JsonProperty("billing-account-id")]
        public billing_Account_Id billing_Account_Id { get; set; }
    }
    public class billing_Account_Id
    {
        [JsonProperty("@scheme-name")]
        public string name { get; set; }
    }

    public class product_offerings
    {
        [JsonProperty("ids")]
        public List<id> ids { get; set; }
    }

    public class Categories
    {
        [JsonProperty("$")]
        public string Value { get; set; }
        [JsonProperty("@list-name")]
        public string listName { get; set; }
    }

    public class line_items
    {
        public payment payment { get; set; }
        [JsonProperty("product-offerings")]
        public List<product_offerings> product_Offerings { get; set; }
        public List<Categories> categories { get; set; }
    }
    }
