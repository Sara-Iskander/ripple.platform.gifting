﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ripple.Platform.Gifting.Clients.Api.Models;

namespace Ripple.Platform.Gifting.Clients.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }
        [HttpPost]
        [Route("service/service-balances")]
        public IActionResult QueryBalance([FromBody]SendGiftParam test)
        {
            //int _intBalanceAmount = 97531;
            int _intBalanceAmount = -2;//500;
            var action = Request.Headers.Where(d => d.Key == "X-Action").SingleOrDefault();
            var messageId = Request.Headers.Where(d => d.Key == "X-MessageID").SingleOrDefault();

            Response.Headers.Add("X-ResultStatus-ErrorCode", "20900");
            Response.Headers.Add("X-ResultStatus-Message", "SUCCESS");
            Response.StatusCode = 204;
            return NoContent();
            //return NotFound();
        }

    }
}
