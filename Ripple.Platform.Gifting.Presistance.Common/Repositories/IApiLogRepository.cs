﻿using Core.Framewrok.Common.Contracts.Persistence.Repository.Write;
using Core.Framewrok.Common.Contracts.Response;
using Ripple.Platform.Gifting.Presistance.Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ripple.Platform.Gifting.Presistance.Common.Repositories
{
    public interface IApiLogRepository<T, TId> : IWriteRepository<T, TId> where T : IApiLog<TId>
    {
        Task<OperationResult<bool>> LogResponse(T updatedEntity);
    }
}
