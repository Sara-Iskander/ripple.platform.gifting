﻿using Core.Framewrok.Common.Contracts.Persistence.Entities.Read;
using Core.Framewrok.Common.Contracts.Persistence.Entities.Write;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ripple.Platform.Gifting.Presistance.Common.Entities
{
    public interface IApiLog<TId> : IWriteEntity<TId>, IReadEntity<TId>
    {
        string RequestUser { get; set; }                    // The user that made the request.

        string RequestMachine { get; set; }                 // The machine that made the request.

        string RequestIpAddress { get; set; }        // The IP address that made the

        string RequestContentType { get; set; }      // The request content type.

        string RequestHeader { get; set; } //request header 

        string RequestContentBody { get; set; }      // The request content body.

        string RequestUri { get; set; }              // The request URI.

        string RequestHttpMethod { get; set; }           // The request method (GET, POST, etc).

        DateTime RequestDateTime { get; set; }     // The request timestamp.

        string ResponseContentType { get; set; }     // The response content type.

        string ResponseHeader { get; set; } //response header 

        string ResponseContentBody { get; set; }     // The response content body.

        int? ResponseStatusCode { get; set; }        // The response status code.

        DateTime? ResponseDateTime { get; set; }    // The response timestamp.
        double Milliseconds { get; set; }//Milliseconds 
    }
}
